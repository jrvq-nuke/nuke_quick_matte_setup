# Quick matte painting Nuke setup

This executable file creates a basic matte painting setup for Nuke, with a camera and card nodes linked to it.

- The number of card nodes can be chosen by the user
- It is possible to add extra cards if needed


It has been tested successfully in **Nuke 11.1v1.**

For more info: www.jaimervq.com
